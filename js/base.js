$(document).ready(function(){
	$('.js-secondary-tabs').tabs();

	$('.c-burger').click(function(event) {
		$('body').toggleClass('menuOpen');
	});

	$('.mat-select select').material_select();

	$('.js-open-popup').click(function(event) {
		$(`#${$(this).data('id')}`).addClass('active');
		$('.c-blur-wrapper').addClass('active')
	});

	$('.js-close-popup').click(function(event) {
		$(`#${$(this).data('id')}`).removeClass('active');
		$('.c-blur-wrapper').removeClass('active')
	});

	$('.datepicker').pickadate({
		formatSubmit: 'yyyy/mm/dd',
		default: 'now'
	});

	if ( $('.js-item-slider').length && $('.js-item-slider').children().length > 4 ) {
		var itemSlider = $('.js-item-slider').bxSlider({
			mode: 'vertical',
			controls: true,
			pager: false,
			minSlides: 4,
			maxSlides: 4,
			moveSlides: 1,
			touchEnabled: false,
			slideMargin: 10,
			nextText: '<i class="e_polka-arrow-bottom"></i>',
			responsive: false
		});

		var setImage = function(url){
			$(".c-item-image").css('background-image', url);
		}

		$(".c-item-slide").click(function(event) {
			setImage( $(this).css('background-image') )
		});

		setImage($('.js-item-slider .c-item-slide').eq(0).css('background-image'))
	};


	if ( $('.js-items-slide').length > 4 ) {
		$(".js-items-slider").owlCarousel({
			items: 4,
			nav: true,
			navText: ['<i class="e_polka-arrow-left"></i>','<i class="e_polka-arrow-right"></i>'],
			loop: true
		});
	};

	$('ul.js-tabs').tabs();

	$('.js-scrollbar').scrollbar();
});
