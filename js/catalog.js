$(document).ready(function(){
  const jsonProducts = require('./products-catalog.json');

  Vue.component('novelties-list-items', {
    props: ['novelties'],
    methods: {
        clearInput: function(id) {
          this.$parent.$options.methods.clearInput(id)
        }
      },
    template: '.js-novelties-list-items'
  })

  Vue.component('novelties-block-items', {
    props: ['novelties'],
    template: '.js-novelties-block-items'
  })

  Vue.component('search-bar', {
    props: ['data'],
    template: '.js-search-bar'
  });

  if ($('.js-novities').length) {
    new Vue({
        el: '.js-novities',
        data: {
          view: 'block',
          noveltiesData: jsonProducts.content,
          string: '',
        },
        methods: {
            getData: function() {
              $.ajax({
                url: $(this).data("ajax-url"),
                method: 'POST',
                async: false,
                data: this.getRequest(),
                contentType: 'application/json',
                success: function(data){
                  console.log('success')
                },
                error: function(){
                  console.log('Error')
                }
              });
            },
            orderBy: function(param, type) {
              this.$data.noveltiesData.sort((a, b) => {
                if (type === 'int') {
                  return a[param] - b[param];
                }else if (type === 'str') {
                  return a[param].toString().localeCompare(b[param]);
                }
              })
            },
            getRequest: function() {
              let checked_brands = [];
              let checked_category;
              $('.js-filter-brands input[type=checkbox]:checked').each(function(){
                checked_brands.push(this.id);
              });
              if ($('.js-filter-categories input[type=checkbox]:checked')[0]) {
                checked_category = $('.js-filter-categories input[type=checkbox]:checked')[0].id
              }

              let request = JSON.stringify(
                {
                  'query': this.$refs.searchbar.value,
                  'selected_shop': $('#selected_shop').val(),
                  'checked_brands': checked_brands,
                  'checked_category': checked_category,
                }
              )

              return request
            },
            getDataDelay: function() {
              this.delay(() => {
                this.getData();
              }, 600 );
            },
            delay: (function(){
              var timer = 0;
              return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
              }
            })(),
            chooseCategory: function(e) {
              if (e.target.checked) {
                $('.js-filter-categories input[type=checkbox]:checked').prop("checked", false)
                $(e.target).prop("checked", true)
              }
            }
          },
    })
  };
})