'use strict';
var webpack = require('webpack');
var WebpackDevServer = require("webpack-dev-server");

var config = {
  context: __dirname,
  entry: {
    js: './js/app.js',
  },
  output: {
    path: __dirname + '/clients/js',
    filename: 'bundles.js',
    chunkFilename: '[id].[chunkhash].js'
  },
  module: {
    rules: [
        {
          test: /\.js$/,
          exclude: [/node_modules/],
          loader: 'babel-loader',
          query: {
            presets: [ "babel-preset-es2015" ].map(require.resolve)
          }
        },
        {
          test: /\.(sass|scss)$/,
          use: [{
            loader: "style-loader"
          }, {
            loader: "css-loader"
          }, {
            loader: "sass-loader"
            
          }]
        },
        {
              test: /\.(jpe?g|png|gif|svg)$/i,
              loaders: [
                  'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                  'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
              ]
          },
        {
          test: /\.(eot|svg|ttf|woff|woff2)$/,
          loader: 'url-loader?limit=100000'
        },
        { 
          test: /\.json$/, 
          loader: "json-loader"
        }
    ]
  },
  devServer: {
    hot: true
  },
  plugins: [
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery',
        'window.$': 'jquery',
    })
 ],
  resolve: {
    alias: {
      vue: './js/libs/vue.min.js',
      jquery: "./js/libs/jquery.min.js"
    }
  },
  devtool: "eval-source-map",
};

module.exports = config;