$("#includeNavigate").load("includes/nav.html"); 
$("#includeHeader").load("includes/header.html"); 
$("#includeTabs").load("includes/tabs.html"); 
$("#includeNovelties").load("includes/catalog/novelties.html"); 
$("#includeCatalogLayout").load("includes/catalog/layout.html"); 
$("#includeReportLayout").load("includes/reports/layout.html"); 
$("#includeOrdersLayout").load("includes/orders/layout.html"); 
$("#includeCatalogSidebar").load("includes/catalog/sidebar.html");
$("#includeReportSidebar").load("includes/reports/sidebar.html");
$("#includeOrdersSidebar").load("includes/orders/sidebar.html");
$("#includeDeliverySidebar").load("includes/delivery/sidebar.html");
$("#includeDeliveryLayout").load("includes/delivery/layout.html");
$("#feedbackPopup").load("includes/feedback_popup.html");
$("#itemInfo").load("includes/item/info.html");
$("#burger").load("includes/burger.html");
$("#itemsSlider").load("includes/items_slider.html");

