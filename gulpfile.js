'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
	gulp.src('./sass/main.sass')
		.pipe(sass())
		.on('error', swallowError)
		.pipe(autoprefixer({
				browsers: ['last 20 versions'],
				cascade: false
			}))
		.pipe(gulp.dest('./suppliers/css'))
});

gulp.task('minify-css', function() {
	return gulp.src('./suppliers/css/main.css')
		.pipe(cssmin())
		.on('error', swallowError)
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./suppliers/css/min'))
});

gulp.task('watch', function () {
	gulp.watch('./sass/**/*.*', ['sass']);
	gulp.watch('./suppliers/css/main.css', ['minify-css']);
});

function swallowError (error) {
	console.log(error.toString());
	this.emit('end');
}